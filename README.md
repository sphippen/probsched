probsched
=========

probsched, The Probabilistic Scheduler V0.1

(c) 2012 Spencer Phippen (spencer.phippen@gmail.com)

Overview
--------
probsched is a program that schedules tasks for the user to do probabilistically, based on a list of tasks to be completed (plus some other per-task data).

It was created out of a self-desire to do a better job of personal time management without reducing the problem to a relatively uninteresting run-of-the-mill personal schedule.  
Plus I like programming.

Each task in the program's task list has:

-   A name, e.g. "probsched V0.3". This is just a name associated with the task for your benefit.
-   A minimum duration, e.g. 30 minutes. This is the minimum amount of time to spend on a task, and can be as low as 0 minutes.
-   A maximum duration, e.g. infinity. This is the maximum amount of time to spend on a task, and can be as high as infinity.
-   A "productivity index", a real number in the range [-2, 2]. This number represents how "good" a task is for you to do, and directly relates to how likely a task is to be scheduled.  
    Values less than 0 represent tasks that are "unproductive" -- for example, in my case, "Play Far Cry" or "Watch Seinfeld".  
    A value of 0 represents a task that is "neutral" in productivity -- for example, in my case, "Clip toenails".  
    Values above zero represent tasks that are "productive" -- for example, in my case, "Numerical Analysis Term Project" or "probsched V0.2".  
-   A due date, e.g. November 30, 2012. This date is intended to be the date by which the task needs to be finished. As the due date gets closer, a task is more likely to be scheduled.

As you may have guessed, tasks with higher productivity indices are scheduled more often than those with lower productivity indices.

probsched's internal scheduling algorithm is being continuously tweaked to provide better results - especially in its early stages (i.e. now), it may provide some strange or unsatisfactory results.

Usage
-----
The program generates schedules by giving you a task to do only when you ask for one. It doesn't plan times for tasks, it simply gives you a task when you run the program.

For usage specifics, run the program with the `--help` option -- however, for starters, `probsched --add` will let you interactively add a task, and `probsched` will select a task for you to perform.

Building/Compilation
--------------------
The repository currently contains an Xcode project file ("osx" folder) and a Visual Studio 2010 solution ("vc2010" folder).

Building from inside the IDEs should be straightforward. Executables are configured to appear in a "bin" folder in the root of the repository.

The code requires the following C++11 libraries and features:

- The `<cstdint>` and `<random>` headers
- C++11 lambda expressions
- C++11 'auto' keyword

License
-------
My code is distributed under the MIT License, see LICENSE.txt.
