//
// File: task.h
// Author: Spencer Phippen
//
// Contains class definition for Task, a class that represents a task to be scheduled.

#ifndef _PROBSCHED_TASK_H_

#include <string>

#include "taskdate.h"
#include "taskduration.h"

namespace probsched
{

// A class that represents a task to be scheduled.
class Task
{
 public:
  // Creates a new Task with the given name, min and max durations, productivity index, and due date.
  //
  // @param name Name of the task.
  // @param minDuration The minimum amount of time to be spent on the task.
  //                    Defaults to 0 minutes, 0 hours.
  // @param maxDuration The maximum amount of time to be spent on the task.
  //                    Defaults to infinity.
  // @param productivityIndex A value representing how productive performing this task would be.
  //                          Must be in the range [-2, 2];
  //                            [-2, 0) is unproductive
  //                            (0, 2] is productive
  //                            0 is neutral
  //                          Defaults to 0.
  // @param dueDate The date by which the task needs to be finished.
  //                Defaults to the invalid date.
  explicit Task(std::string name, TaskDuration minDuration = TaskDuration::finiteDuration(0, 0), TaskDuration maxDuration = TaskDuration::infiniteDuration(), double productivityIndex = 0, TaskDate dueDate = TaskDate::invalidDate());
  
  // Returns the name of this task.
  std::string name() const;
  // Sets the name of this task to |name|.
  void setName(std::string name);

  // Returns the minimum duration of this task.
  TaskDuration minimumDuration() const;
  // Sets the minimum duration of this task to |dur|.
  //
  // @param dur The new minimum duration.
  //            Must be <= the current maximumDuration.
  void setMinimumDuration(TaskDuration dur);

  // Returns the maximum duration of this task.
  TaskDuration maximumDuration() const;
  // Sets the maximum duration of this task to |dur|.
  //
  // @param dur The new maximum duration.
  //            Must be >= the current minimumDuration.
  void setMaximumDuration(TaskDuration dur);

  // Returns the productivity index of this task.
  double productivityIndex() const;
  // Sets the productivity index of this task to |productivityIndex|.
  //
  // @param productivityIndex The new productivity index.
  //                          Must be in range [-2, 2].
  void setProductivityIndex(double productivityIndex);

  // Returns the due date of this task.
  TaskDate dueDate() const;
  // Sets the due date of this task to |dueDate|.
  // @param dueDate The new due date.
  void setDueDate(TaskDate dueDate);

  // Returns the "adjusted" productivity index of this task, which is a function of:
  // - This task's productivity index
  // - This tasks' due date
  // - The current date
  // As the current date approaces the due date of the task, the adjusted productivity index increases.
  double adjustedProductivityIndex() const;

 private:
  std::string m_name;
  TaskDuration m_minDuration;
  TaskDuration m_maxDuration;
  double m_productivity;
  TaskDate m_dueDate;
};

std::string durationStringForTask(const Task& task);

} // namespace probsched

#endif // _PROBSCHED_TASK_H_
