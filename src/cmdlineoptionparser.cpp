//
// File: cmdlineoptionparser.cpp
// Author: Spencer Phippen
//
// Contains function definitions for the CmdLineOptionParser class.

#include "cmdlineoptionparser.h"

#include <cassert>

using std::string;
using std::vector;

namespace probsched
{

// Private getMan
 
// "Covers" |argCount| arguments starting at index |index| in |argv|.
// Modifies |argc|, and |argv| appropriately.
// @param argCount Number of arguments to cover
// @param index Points to the index to start covering at.
// @param argc Points to the number of arguments in |argv|.
//             After the function call, contains the new number of arguments in |argv|.
// @param argv Contains pointers to argument strings.
//             After the function call, contains the same string addresses,
//             except |argCount| ones starting at index |index| have been covered
//             by arguments shifted backward in the array.
static void coverArguments(int argCount, int index, int* argc, char* argv[]);

CmdLineOptionParser::CmdLineOptionParser() :
    m_mandatoryArgumentCount(-1), m_ignoreUnrecognizedOptions(false), m_ignoreRepeatedOptions(false), m_nextId(0), m_hasProcessed(false), m_successfulParse(false) { }

int CmdLineOptionParser::addOption(char flag, string longFlag, bool hasArgument)
{
  assert(!m_hasProcessed);

  // One of short or long flags must be specified
  if (flag == '\0' && longFlag.size() == 0) {
    return -1;
  }

  if (flag != '\0') {
    m_shortToId[flag] = m_nextId;
  }
  if (longFlag.size() > 0) {
    m_longToId[longFlag] = m_nextId;
  }
  m_idToHasArgument[m_nextId] = hasArgument;

  return m_nextId++;
}

int CmdLineOptionParser::mandatoryArgumentCount() const
{
  return m_mandatoryArgumentCount;
}

void CmdLineOptionParser::setMandatoryArgumentCount(int count)
{
  assert(!m_hasProcessed);
  assert(count >= -1);

  m_mandatoryArgumentCount = count;
}

bool CmdLineOptionParser::doesIgnoreUnrecognizedOptions() const
{
  return m_ignoreUnrecognizedOptions;
}

void CmdLineOptionParser::ignoreUnrecognizedOptions(bool ignore)
{
  assert(!m_hasProcessed);
  m_ignoreUnrecognizedOptions = ignore;
}

bool CmdLineOptionParser::doesIgnoreRepeatedOptions() const
{
  return m_ignoreRepeatedOptions;
}

void CmdLineOptionParser::ignoreRepeatedOptions(bool ignore)
{
  assert(!m_hasProcessed);
  m_ignoreRepeatedOptions = ignore;
}

void CmdLineOptionParser::parseArguments(int* argc, char* argv[])
{
  assert(!m_hasProcessed);

  if (argc == NULL) {
    m_successfulParse = false;
    m_hasProcessed = true;
    m_errorString = "NULL |argc| passed to parsing function.";
    return;
  }
  else if (argv == NULL) {
    m_successfulParse = false;
    m_hasProcessed = true;
    m_errorString = "NULL |argv| passed to parsing function.";
    return;
  }

  // Extract options, ignoring first one (exec name)
  for (int i = 1; i < *argc; i++) {
    char* arg = argv[i];
    // if this is an option
    if (arg[0] == '-') {
      // if this is a long option
      if (arg[1] == '-') {
        parseLongOption(&i, argc, argv);
        if (m_hasProcessed) {
          return;
        }
      }
      // if this is a short option
      else if (arg[1] != '\0') {
        parseShortOption(&i, argc, argv);
        if (m_hasProcessed) {
          return;
        }
      }
    }
  }

  // If we're paying attention to # of arguments
  if (m_mandatoryArgumentCount != -1) {
    // Verify for the correct number of mandatory arguments, accounting for exec name
    if (*argc - 1 != m_mandatoryArgumentCount) {
      m_successfulParse = false;
      m_errorString = "Not enough arguments specified.";
      m_hasProcessed = true;
      return;
    }
  }

  // If we've made it this far, the parse was successful
  m_successfulParse = true;
  m_hasProcessed = true;
}

bool CmdLineOptionParser::parseWasSuccessful() const
{
  assert(m_hasProcessed);
  return m_successfulParse;
}

string CmdLineOptionParser::parseError() const
{
  return m_errorString;
}

int CmdLineOptionParser::idForOptionWithShortFlag(char flag) const
{
  if (m_shortToId.count(flag) == 1) {
    return m_shortToId.at(flag);
  }
  else {
    return -1;
  }
}

int CmdLineOptionParser::idForOptionWithLongFlag(std::string longFlag) const
{
  if (m_longToId.count(longFlag) == 1) {
    return m_longToId.at(longFlag);
  }
  else {
    return -1;
  }
}

bool CmdLineOptionParser::optionWithIdWasPassed(int optionId) const
{
  assert(m_hasProcessed);
  return m_idToArgument.count(optionId) == 1;
}

string CmdLineOptionParser::argumentForOptionWithId(int optionId) const
{
  assert(m_hasProcessed);
  // Return the argument if the option mapping exists
  if (m_idToHasArgument.count(optionId) == 1 && m_idToHasArgument.at(optionId) == true && m_idToArgument.count(optionId) == 1) {
    return m_idToArgument.at(optionId);
  }
  else {
    return "";
  }
}

void CmdLineOptionParser::parseShortOption(int* optIndex, int* argc, char* argv[])
{
  char* arg = argv[*optIndex];
  char shortFlag = arg[1];
  // if this option was specified with a call to |addOption|
  if (m_shortToId.count(shortFlag) == 1) {
    int optionId = m_shortToId[shortFlag];
    // if this option was already specified and we are not ignoring duplicate options
    if (!m_ignoreRepeatedOptions && m_idToArgument.count(optionId) == 1) {
      m_successfulParse = false;
      m_errorString = "Repeated option \"-";
      m_errorString += shortFlag;
      m_errorString += "\".";
      m_hasProcessed = true;
      return;
    }
    // if this option has an argument
    if (m_idToHasArgument[optionId]) {
      // if this option's argument is specified in the same argv entry
      if (arg[2] != '\0') {
        string optionArgument(&arg[2]);
        m_idToArgument[optionId] = optionArgument;
        // "Cover" the argument in argv and modify argc appropriately
        coverArguments(1, *optIndex, argc, argv);
        // Move index back so that next iteration moves it to "next argument"
        (*optIndex)--;
      }
      // if this option's argument is not specified in the same argv entry
      else {
        // if this option is at the end of the argument array,
        // i.e. we are expecting an argument but there isn't one after this one
        if (*optIndex == *argc - 1) {
          m_successfulParse = false;
          m_errorString = "Missing argument for \"-";
          m_errorString += shortFlag;
          m_errorString += "\" option.";
          m_hasProcessed = true;
          return;
        }

        // At this point, we know the next string is an argument
        string optionArgument(argv[*optIndex+1]);
        m_idToArgument[optionId] = optionArgument;
        coverArguments(2, *optIndex, argc, argv);
        // Move index back so that next iteration moves it to "next argument"
        (*optIndex)--;
      }
    }
    // if this option does not have an argument
    else {
      m_idToArgument[optionId] = string("");
      coverArguments(1, *optIndex, argc, argv);
      (*optIndex)--;
    }
  }
  // if this option was not specified and we are not ignoring unrecognized options
  else if (!m_ignoreUnrecognizedOptions) {
    m_successfulParse = false;
    m_errorString = "Unrecognized option \"-";
    m_errorString += shortFlag;
    m_errorString += "\".";
    m_hasProcessed = true;
    return;
  }
}

void CmdLineOptionParser::parseLongOption(int* optIndex, int* argc, char* argv[])
{
  string longFlag = &argv[*optIndex][2];
  // if this option was specified with a call to |addOption|
  if (m_longToId.count(longFlag) == 1) {
    int optionId = m_longToId[longFlag];
    // if this option was already specified and we are not ignoring duplicate options
    if (!m_ignoreRepeatedOptions && m_idToArgument.count(optionId) == 1) {
      m_successfulParse = false;
      m_errorString = "Repeated option \"-";
      m_errorString += longFlag;
      m_errorString += "\".";
      m_hasProcessed = true;
      return;
    }
    // if this option has an argument
    if (m_idToHasArgument[optionId]) {
      // if this option is at the end of the argument array,
      // i.e. we are expecting an argument but there isn't one after this one
      if (*optIndex == *argc - 1) {
        m_successfulParse = false;
        m_errorString = "Missing argument for \"--";
        m_errorString += longFlag;
        m_errorString += "\" option.";
        m_hasProcessed = true;
        return;
      }

      // At this point, we know the next string is an argument
      string optionArgument(argv[*optIndex+1]);
      m_idToArgument[optionId] = optionArgument;
      coverArguments(2, *optIndex, argc, argv);
      // Move index back so that next iteration moves it to "next argument"
      (*optIndex)--;
    }
    // if this option does not have an argument
    else {
      m_idToArgument[optionId] = string("");
      coverArguments(1, *optIndex, argc, argv);
      (*optIndex)--;
    }
  }
  // if this option was not specified and we are not ignoring unrecognized options
  else if (!m_ignoreUnrecognizedOptions) {
    m_successfulParse = false;
    m_errorString = "Unrecognized option \"--";
    m_errorString += longFlag;
    m_errorString += "\".";
    m_hasProcessed = true;
    return;
  }
}

void coverArguments(int argCount, int index, int* argc, char* argv[])
{
  // Make sure we have at least |argCount| arguments to cover up
  assert(index + argCount <= *argc);
  assert(argc != NULL);
  assert(argv != NULL);

  // Shift other arguments over as necessary
  int offset = 0;
  while (offset+index+argCount < *argc) {
    argv[index+offset] = argv[index+argCount+offset];
    offset++;
  }

  *argc -= argCount;
}

} // namespace probsched
