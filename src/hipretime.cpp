//
// File: hipretime.cpp
// Author: Spencer Phippen
//
// Contains implementations of high-precision time related functions.

#ifdef _WIN32
#define WINDOWS_PLATFORM
#elif __APPLE__
#define MAC_PLATFORM
#endif

#include "hipretime.h"

#ifdef WINDOWS_PLATFORM
#include <Windows.h>
#elif defined MAC_PLATFORM
#include <sys/time.h>
#endif

namespace probsched
{

uint32_t highPrecisionTime()
{
#ifdef WINDOWS_PLATFORM
  LARGE_INTEGER time;
  QueryPerformanceCounter(&time);
  return time.LowPart;
#elif defined MAC_PLATFORM
  timeval time;
  gettimeofday(&time, NULL);
  return time.tv_usec;
#endif
}

} // namespace probsched