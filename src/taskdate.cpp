//
// File: date.cpp
// Author: Spencer Phippen
//
// Contains function definitions for the TaskDate class.

#include "taskdate.h"

#include <ctime>
#include <cassert>
#include <sstream>

using std::string;
using std::ostringstream;

static int monthDays[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
static const char* monthAbbreviations[12] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

static bool isLeapYear(int year)
{
  return (year % 4 == 0) && ((year % 100 != 0) || (year % 400 == 0));
}

namespace probsched
{

TaskDate TaskDate::validDate()
{
  time_t currentTime = time(NULL);
  tm* calendarTime = localtime(&currentTime);
  // The tm struct stores a day in the range [1, 31], so we bring it down to [0, 30]
  int day = calendarTime->tm_mday - 1;
  int month = calendarTime->tm_mon;
  // The tm struct stores a year that is the number of years since 1900;
  // hence we add 1900 to readjust.
  int year = calendarTime->tm_year + 1900;
  
  return TaskDate(day, month, year, true);
}

TaskDate TaskDate::validDate(int day, int month, int year)
{
  return TaskDate(day, month, year, true);
}

TaskDate TaskDate::invalidDate()
{
  return TaskDate(-1, -1, -1, false);
}

TaskDate::TaskDate(int day, int month, int year, bool valid) : m_day(day), m_month(month), m_year(year)
{
  assert(!valid || validDateTriple(day, month, year));
}

bool TaskDate::isInvalid() const
{
  return m_day == -1;
}

int TaskDate::day() const
{
  return m_day;
}

int TaskDate::month() const
{
  return m_month;
}

int TaskDate::year() const
{
  return m_year;
}

bool validDateTriple(int day, int month, int year)
{
  bool validSigns = (day >= 0) && (month >= 0);
  bool validMonth = month < 12;
  
  // We must return here to avoid using month as an array index below
  if (!validSigns || !validMonth) {
    return false;
  }

  bool validDayForMonthAndYear = false;
  // If we're in February in a leap year
  if (month == 1 && isLeapYear(year)) {
    validDayForMonthAndYear = day < monthDays[month]+1;
  }
  else {
    validDayForMonthAndYear = day < monthDays[month];
  }
  return validSigns && validMonth && validDayForMonthAndYear;
}

int numberOfDaysDifference(const TaskDate& date1, const TaskDate& date2)
{
  tm time1, time2;
  memset(&time1, 0, sizeof(time1));
  time1.tm_mday = date1.day();
  time1.tm_mon = date1.month();
  // Adjust by 1900 because the tm struct's year field holds the number of years after 1990
  time1.tm_year = date1.year() - 1900;
  
  memset(&time2, 0, sizeof(time2));
  time2.tm_mday = date2.day();
  time2.tm_mon = date2.month();
  // Adjust as above
  time2.tm_year = date2.year() - 1900;
  
  double secondsDifference = difftime(mktime(&time1), mktime(&time2));
  
  // Round to nearest number of days.
  // The seconds difference should be exact, but after division, well, you know how floating point is
  return static_cast<int>((secondsDifference / (60.0 * 60.0 * 24.0)) + 0.5);
}

string stringFromTaskDate(const TaskDate& td)
{
  // If the date is valid
  if (td.isInvalid()) {
    return string("N/A");
  }
  else {
    ostringstream dateStream;
    dateStream << monthAbbreviations[td.month()] << ' ' << td.day()+1 << ' ' << td.year();
    return dateStream.str();
  }
}

} // namespace probsched