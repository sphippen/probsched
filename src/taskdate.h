//
// File: date.h
// Author: Spencer Phippen
//
// Contains class definition for TaskDate, a class that represents a date associated with a Task.

#ifndef _PROBSCHED_TASK_DATE_H_
#define _PROBSCHED_TASK_DATE_H_

#include <string>

#include <ctime>

namespace probsched
{

class TaskDate
{
 public:
  // Creates and returns a new Date, holding the current date.
  static TaskDate validDate();
  
  // Creates a new TaskDate with the given day, month, and year.
  //
  // @param day The day of the date to be created (zero-indexed, e.g. 0-29 for April).
  //            Must be in a valid number of days for the given month and year - I'm not going to list the values here because they're very well-known.
  // @param month The month of the date to be created.
  //              Must be in range [0,11].
  // @param year The year of the date to be created.
  static TaskDate validDate(int day, int month, int year);

  // Returns a Date marked as "invalid".
  static TaskDate invalidDate();

  // Returns true if this TaskDate is an invalid date, i.e. was created with the TaskDate::invalidDate function.
  bool isInvalid() const;

  // Returns the day of the this date.
  int day() const;

  // Returns the month of this date.
  int month() const;

  // Returns the year of this date.
  int year() const;

 private:
  TaskDate(int day, int month, int year, bool valid);
  int m_day;
  int m_month;
  int m_year;
};

// Returns the number of days from date2 to date 1, that is,
// (date1 - date2) expressed as a number of days.
// @param date1 The minuend of the time difference operation.
// @param date2 The subtrahend of the time difference operation.
int numberOfDaysDifference(const TaskDate& date1, const TaskDate& date2);

// Returns true if the given date triple is valid.
// @param day The day of the triple to be tested.
// @param month The month of the triple to be tested.
// @param year The year of the triple to be tested.
bool validDateTriple(int day, int month, int year);

// Returns a string representation of the given date.
// @param td The TaskDate to generate a string representation of.
std::string stringFromTaskDate(const TaskDate& td);

} // namespace probsched

#endif // _PROBSCHED_TASK_DATE_H_
