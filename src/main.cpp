//
// File: main.cpp
// Author: Spencer Phippen
//
// Contains the program entry point and much of the program logic.

#include <cstdio>
#include <iostream>
#include <cmath>
#include <cassert>
#include <fstream>
#include <string>
#include <vector>
#include <random>
#include <algorithm>

#include "cmdlineoptionparser.h"
#include "task.h"
#include "taskduration.h"
#include "taskdate.h"
#include "hipretime.h"

using std::ifstream;
using std::ofstream;
using std::cin;
using std::string;
using std::vector;
using std::remove_if;
using std::for_each;
using std::find_if;
using std::mt19937;
using std::getline;
using std::uniform_real_distribution;
using probsched::CmdLineOptionParser;
using probsched::Task;
using probsched::TaskDuration;
using probsched::TaskDate;
using probsched::validDateTriple;
using probsched::highPrecisionTime;
using probsched::durationStringForTask;
using probsched::stringFromTaskDate;

const static char* defaultFile = "tasks.dat";

static void printHelp();
static void printVersion();
static bool readTasks(vector<Task>* tasksToFill, string filename);
static bool writeTasks(const vector<Task>* tasksToWrite, string filename);

static void listTasks(const vector<Task>* tasks);
static void addTask(vector<Task>* tasks);
static void selectTask(vector<Task>* tasks, double maxHours);

int main(int argc, char* argv[])
{
  // Grab command line arguments
  CmdLineOptionParser parser;

  // Operation arguments
  int helpId = parser.addOption('\0', "help", false);
  int versionId = parser.addOption('v', "version", false);
  int listId = parser.addOption('l', "list", false);
  int addId = parser.addOption('a', "add", false);
  int removeId = parser.addOption('r', "remove", true);

  // Other arguments
  // Default operation arguments
  int minutesId = parser.addOption('m', "minutes", true);
  int hoursId = parser.addOption('h', "hours", true);
  // Operation-independent arguments
  int fileId = parser.addOption('f', "file", true);

  parser.parseArguments(&argc, argv);
  
  // Make sure the parse went well
  if (!parser.parseWasSuccessful()) {
    fprintf(stderr, "ERROR: %s\nPass \"--help\" for usage info.\n", parser.parseError().c_str());
    return 1;
  }

  // Print help and exit if option was passed
  bool doHelp = parser.optionWithIdWasPassed(helpId);
  if (doHelp) {
    printHelp();
    return 0;
  }

  // Print version info, if applicable
  bool doVersion = parser.optionWithIdWasPassed(versionId);
  if (doVersion) {
    printVersion();
    return 0;
  }

  // We need to load tasks for all the other operations, so determine the data file
  string taskFile = parser.argumentForOptionWithId(fileId);
  if (taskFile.size() == 0) {
    taskFile = defaultFile;
  }

  // Actually load the tasks
  vector<Task> tasks;
  if (!readTasks(&tasks, taskFile)) {
    fprintf(stderr, "ERROR: Task data file \"%s\" could not be read or is invalid.\n", taskFile.c_str());
    return 1;
  }

  bool doList = parser.optionWithIdWasPassed(listId);
  if (doList) {
    listTasks(&tasks);
    return 0;
  }

  bool doAdd = parser.optionWithIdWasPassed(addId);
  if (doAdd) {
    addTask(&tasks);
    writeTasks(&tasks, taskFile);
    return 0;
  }

  bool doRemove = parser.optionWithIdWasPassed(removeId);
  if (doRemove) {
    string removeArg = parser.argumentForOptionWithId(removeId);
    int removeIndex;
    int result = sscanf(removeArg.c_str(), " %d", &removeIndex);
    if (result != 1 || removeIndex >= tasks.size()) {
      fprintf(stderr, "ERROR: Bad taskIndex argument \"%d\"\n", removeIndex);
      return 1;
    }
    
    // Remove the specified task and write them out to disk
    tasks.erase(tasks.begin() + removeIndex);
    writeTasks(&tasks, taskFile);
    return 0;
  }

  // If we've reached this point, we need to select a task
  double maxHours = -1.0;
  string minutesArg = parser.argumentForOptionWithId(minutesId);
  if (minutesArg.size() > 0) {
    // Actually attempt to extract a number of minutes
    double minutes;
    int result = sscanf(minutesArg.c_str(), " %lf", &minutes);
    // If the parse was sucessful
    if (result == 1) {
      maxHours = minutes / 60.0;
    }
    else {
      fprintf(stderr, "ERROR: Bad argument \"%s\" to \"-m\" option.\n", minutesArg.c_str());
      return 1;
    }
  }
  else {
    // Attempt to extract the hours parameter
    string hoursArg = parser.argumentForOptionWithId(hoursId);
    if (hoursArg.size() > 0) {
      double hours;
      int result = sscanf(hoursArg.c_str(), " %lf", &hours);
      // If the parse was successful
      if (result == 1) {
        maxHours = hours;
      }
      else {
        fprintf(stderr, "ERROR: Bad argument \"%s\" to \"-h\" option.\n", hoursArg.c_str());
        return 1;
      }
    }
  }
  
  selectTask(&tasks, maxHours);
  
  return 0;
}

static void printHelp()
{
  printf("Usages:\n"
         "probsched <options>\n"
         "\tGenerates a task for you to do.\n"
         "probsched [-l/--list] <options>\n"
         "\tLists all possible tasks.\n"
         "probsched [-a/--add] <options>\n"
         "\tInteractively adds a task to the list of all possible tasks.\n"
         "probsched [-r/--remove] <options> taskIndex\n"
         "\tRemoves a task with the specified taskIndex from the list of all possible tasks.\n"
         "\n"
         "Options:\n"
         "\t--help\n"
         "\t\tPrints this help message.\n"
         "\n"
         "\t-v,--version\n"
         "\t\tPrints a version number notices.\n"
         "\n"
         "\t-m,--minutes\n"
         "\t\tGives a maximum (integer) number of minutes to consider when generating a task.\n"
         "\t\tMutually exclusive with the [-h/--hours] option.\n"
         "\t\tBy default, there is no maximum.\n"
         "\n"
         "\t-h,--hours\n"
         "\t\tGives a maximum (decimal point) number of hours to consider when generating a task.\n"
         "\t\tMutually exclusive with the [-m/--minutes] option.\n"
         "\t\tBy default, there is no maximum.\n"
         "\n"
         "\t-f,--file\n"
         "\t\tGives a file to use as the list of events. Defaults to \"tasks.dat\".\n");
}

static void printVersion()
{
  printf("probsched, The Probabilistic Scheduler V0.1\n"
         "(c) 2012 Spencer Phippen\n");
}

static bool readTasks(vector<Task>* tasksToFill, string filename)
{
  ifstream inFile(filename.c_str());
  if (!inFile) {
    return false;
  }

  // Read the number of tasks
  int taskCount;
  inFile >> taskCount;
  if (!inFile) {
    return false;
  }

  // Read each task
  for (int i = 0; i < taskCount; i++) {
    // Task name
    string taskName;
    // Clear out the old line
    getline(inFile, taskName);
    getline(inFile, taskName);

    // Task min/max durations
    TaskDuration minDuration = TaskDuration::infiniteDuration();
    TaskDuration maxDuration = TaskDuration::infiniteDuration();
    
    int minDurationHours, minDurationMinutes;
    inFile >> minDurationHours;
    if (minDurationHours >= 0) {
      inFile >> minDurationMinutes;
      if (minDurationMinutes < 0) {
        return false;
      }
      minDuration = TaskDuration::finiteDuration(minDurationHours, minDurationMinutes);
    }

    int maxDurationHours, maxDurationMinutes;
    inFile >> maxDurationHours;
    if (maxDurationHours >= 0) {
      inFile >> maxDurationMinutes;
      if (maxDurationMinutes < 0) {
        return false;
      }
      maxDuration = TaskDuration::finiteDuration(maxDurationHours, maxDurationMinutes);
    }

    // Task productivity index
    double productivityIndex;
    inFile >> productivityIndex;

    // Due date
    TaskDate dueDate = TaskDate::invalidDate();
    int day, month, year;
    inFile >> day;
    if (day >= 0) {
      inFile >> month >> year;
      if (validDateTriple(day, month, year)) {
        dueDate = TaskDate::validDate(day, month, year);
      }
      else {
        return false;
      }
    }

    tasksToFill->push_back(Task(taskName, minDuration, maxDuration, productivityIndex, dueDate));

    if (!inFile) {
      return false;
    }
  }
  
  return true;
}

static bool writeTasks(const vector<Task>* tasksToWrite, string filename)
{
  ofstream outFile(filename.c_str());
  if (!outFile) {
    return false;
  }
  
  outFile << tasksToWrite->size() << '\n';
  
  for (auto i = tasksToWrite->begin(); i != tasksToWrite->end(); i++) {
    Task toWrite = *i;

    // Task name
    outFile << toWrite.name() << '\n';
    
    // Minimum task duration
    if (toWrite.minimumDuration().isInfinite()) {
      outFile << -1 << ' ';
    }
    else {
      outFile << toWrite.minimumDuration().hours() << ' ' << toWrite.minimumDuration().minutes() << ' ';
    }
    
    // Maximum task duration
    if (toWrite.maximumDuration().isInfinite()) {
      outFile << -1 << ' ';
    }
    else {
      outFile << toWrite.maximumDuration().hours() << ' ' << toWrite.maximumDuration().minutes() << ' ';
    }
    
    // Productivity index
    outFile << toWrite.productivityIndex() << ' ';
    
    // Due date
    if (toWrite.dueDate().isInvalid()) {
      outFile << -1 << '\n';
    }
    else {
      outFile << toWrite.dueDate().day() << ' ' << toWrite.dueDate().month() << ' ' << toWrite.dueDate().year() << '\n';
    }
  }
  
  if (!outFile) {
    return false;
  }
  
  return true;
}

// THE REAL WORKHORSE FUNCTIONS
static void listTasks(const vector<Task>* tasks)
{
  for (vector<Task>::size_type i = 0; i < tasks->size(); ++i) {
    Task task = (*tasks)[i];
    printf("Index: %d\n", static_cast<int>(i));
    printf("Name: %s\n", task.name().c_str());
    printf("Duration: %s\n", durationStringForTask(task).c_str());
    printf("Productivity: %f\n", task.productivityIndex());
    printf("Due Date: %s\n", stringFromTaskDate(task.dueDate()).c_str());
    
    if (i != (tasks->size() - 1)) {
      printf("\n");
    }
  }
}

// A VARIETY OF INPUT-RELATED HELPER FUNCTIONS
static bool validMinimumDurationLine(string inputLine)
{
  int hours, minutes;
  int result = sscanf(inputLine.c_str(), " %d %d", &hours, &minutes);
  if (result > 0) {
    if (hours == -1) {
      return true;
    }
    
    return (result == 2) && (hours >= 0) && (minutes >= 0);
  }
  else {
    return false;
  }
}

static bool validMaximumDurationLine(string inputLine, TaskDuration minimumDuration)
{
  int hours, minutes;
  int result = sscanf(inputLine.c_str(), " %d %d", &hours, &minutes);
  if (result > 0) {
    if (hours == -1) {
      return true;
    }
    
    // This _should_ never happen, because the minimumDuration is provided by user input
    assert(!minimumDuration.isInfinite());
    
    return TaskDuration::finiteDuration(hours, minutes) >= minimumDuration;
  }
  return false;
}

static bool validProductivityIndexLine(string inputLine)
{
  double productivityIndex;
  int result = sscanf(inputLine.c_str(), " %lf", &productivityIndex);
  return (result == 1) && (productivityIndex >= -2.0) && (productivityIndex <= 2.0);
}

static bool validDueDateLine(string inputLine)
{
  int day, month, year;
  int result = sscanf(inputLine.c_str(), " %d %d %d", &day, &month, &year);
  if (result > 0) {
    if (day == -1) {
      return true;
    }
    
    return (result == 3) && validDateTriple(day-1, month-1, year);
  }
  return false;
}

static TaskDuration minimumDurationFromInputLine(string inputLine)
{
  int hours, minutes;
  int result = sscanf(inputLine.c_str(), " %d %d", &hours, &minutes);
  if (result > 0) {
    if (hours == -1) {
      return TaskDuration::finiteDuration(0, 0);
    }
    else if (result == 2 && hours >= 0 && minutes >= 0) {
      return TaskDuration::finiteDuration(hours, minutes);
    }
  }
  
  // This is an error case, but this function doesn't need to provide that information - that's what the validation function is for
  return TaskDuration::infiniteDuration();
}

static TaskDuration maximumDurationFromInputLine(string inputLine)
{
  int hours, minutes;
  int result = sscanf(inputLine.c_str(), " %d %d", &hours, &minutes);
  if (result > 0) {
    if (hours == -1) {
      return TaskDuration::infiniteDuration();
    }
    else if (result == 2 && hours >= 0 && minutes >= 0) {
      return TaskDuration::finiteDuration(hours, minutes);
    }
  }
  
  // This is an error case, but this function doesn't need to provide that information - that's what the validation function is for
  return TaskDuration::infiniteDuration();
}

static double productivityIndexFromInputLine(string inputLine)
{
  double productivityIndex;
  int result = sscanf(inputLine.c_str(), " %lf", &productivityIndex);
  if (result == 1) {
    return productivityIndex;
  }
  // Might as well return something that won't kill the program, even though this case should never run
  else {
    return 0.0;
  }
}

static TaskDate dueDateFromInputLine(string inputLine)
{
  int day, month, year;
  int result = sscanf(inputLine.c_str(), " %d %d %d", &day, &month, &year);
  if (result > 0) {
    if (day == -1) {
      return TaskDate::invalidDate();
    }
    else if (result == 3 && validDateTriple(day-1, month-1, year)) {
      return TaskDate::validDate(day-1, month-1, year);
    }
  }
  // This is an error case, but this function doesn't need to provide that information - that's what the validation function is for
  return TaskDate::invalidDate();
}
// END OF INPUT-RELATED HELPER FUNCTIONS

static void addTask(vector<Task>* tasks)
{
  string inputLine;
  do {
    printf("Task name (must not be empty): ");
    getline(cin, inputLine);
  } while (cin && (inputLine.size() == 0));
  if (inputLine.size() == 0) {
      return;
  }
  string taskName = inputLine;
  
  do {
    printf("Minimum duration (h m, or -1 for none): ");
    getline(cin, inputLine);
  } while (cin && !validMinimumDurationLine(inputLine));
  if (!validMinimumDurationLine(inputLine)) {
      return;
  }
  TaskDuration minimumDuration = minimumDurationFromInputLine(inputLine);
  
  do {
    printf("Maximum duration, no smaller than above (h m, or -1 for none): ");
    getline(cin, inputLine);
  } while (cin && !validMaximumDurationLine(inputLine, minimumDuration));
  if (!validMaximumDurationLine(inputLine, minimumDuration)) {
      return;
  }
  TaskDuration maximumDuration = maximumDurationFromInputLine(inputLine);
  
  do {
    printf("Productivity index (real number in [-2, 2]): ");
    getline(cin, inputLine);
  } while (cin && !validProductivityIndexLine(inputLine));
  if (!validProductivityIndexLine(inputLine)) {
      return;
  }
  double productivityIndex = productivityIndexFromInputLine(inputLine);
  
  do {
    printf("Due date (d m y, or -1 for none): ");
    getline(cin, inputLine);
  } while (cin && !validDueDateLine(inputLine));
  if (!validDueDateLine(inputLine)) {
      return;
  }
  TaskDate dueDate = dueDateFromInputLine(inputLine);
  
  tasks->push_back(Task(taskName, minimumDuration, maximumDuration, productivityIndex, dueDate));
}

static void selectTask(vector<Task>* tasks, double maxHours)
{
  bool restrictTime = maxHours >= 0.0;
  
  if (restrictTime) {
    // Filter out tasks that are too long
    auto maxDuration = TaskDuration::finiteDuration(maxHours);
    
    // Remove tasks that are too long
    tasks->erase(remove_if(tasks->begin(), tasks->end(), [maxDuration] (Task task) {
      return task.minimumDuration() > maxDuration;
    }), tasks->end());
    
    // Clamp remaining tasks' max durations to the user-specified one
    for_each(tasks->begin(), tasks->end(), [maxDuration] (Task& task) {
      if (task.maximumDuration() > maxDuration) {
        task.setMaximumDuration(maxDuration);
      }
    });
    
    tasks->erase(remove_if(tasks->begin(), tasks->end(), [maxDuration] (Task task) {
      return task.maximumDuration() == TaskDuration::finiteDuration(0, 0);
    }), tasks->end());
  }
  
  // If there actually any tasks to select from
  if (tasks->size() > 0) {
    // At this point, we have an array of Tasks with minimum and maximum durations less than the user-specified one
    // Calculate a probability normalization factor
    double probabilityTotal = 0.0;
    for_each(tasks->begin(), tasks->end(), [&probabilityTotal] (Task task) {
      probabilityTotal += pow(2.0, task.adjustedProductivityIndex());
    });
    // Select a task by generating a random number in a uniform distribution of [0, 1] and walking through the list of Tasks
    mt19937 rng;
    rng.seed(static_cast<mt19937::result_type>(highPrecisionTime()));
    uniform_real_distribution<> distr;
    double randomValue = distr(rng);
    randomValue *= probabilityTotal;
    
    auto selected = find_if(tasks->begin(), tasks->end(), [randomValue,&probabilityTotal] (Task task) -> bool {
      probabilityTotal -= pow(2.0, task.adjustedProductivityIndex());
      return probabilityTotal <= randomValue;
    });
    // We should have found an actual task
    assert(selected != tasks->end());
    
    // Print the details of the selected task
    Task selectedTask = *selected;
    printf("Your task:\n"
           "%s\n"
           "Duration: %s\n", selectedTask.name().c_str(), durationStringForTask(selectedTask).c_str());
  }
  else {
    printf("There are no tasks that meet the given time criteria.\n");
  }
}
