//
// File: task.cpp
// Author: Spencer Phippen
//
// Contains function definitions for the Task class.

#include "task.h"

#include <cassert>

namespace probsched
{

using std::string;

Task::Task(string name, TaskDuration minDuration, TaskDuration maxDuration, double productivityIndex, TaskDate dueDate) :
    m_name(name), m_minDuration(minDuration), m_maxDuration(maxDuration), m_productivity(productivityIndex), m_dueDate(dueDate) { }

string Task::name() const
{
  return m_name;
}
void Task::setName(string name)
{
  m_name = name;
}

TaskDuration Task::minimumDuration() const
{
  return m_minDuration;
}
void Task::setMinimumDuration(TaskDuration dur)
{
  assert(dur <= m_maxDuration);
  m_minDuration = dur;
}

TaskDuration Task::maximumDuration() const
{
  return m_maxDuration;
}
void Task::setMaximumDuration(TaskDuration dur)
{
  assert(dur >= m_minDuration);
  m_maxDuration = dur;
}

double Task::productivityIndex() const
{
  return m_productivity;
}
void Task::setProductivityIndex(double productivityIndex)
{
  assert(productivityIndex >= -2.0 && productivityIndex <= 2.0);
  m_productivity = productivityIndex;
}

TaskDate Task::dueDate() const
{
  return m_dueDate;
}
void Task::setDueDate(TaskDate dueDate)
{
  m_dueDate = dueDate;
}

double Task::adjustedProductivityIndex() const
{
  // If there isn't a due date, the productivity index is unchanged
  if (m_dueDate.isInvalid()) {
    return m_productivity;
  }
  
  // The productivity index to use for an item that has reached its due date
  const static int dueProductivityIndex = 10;
  // The number of days before the due date to begin adjusting the productivity index
  const static int dueDateAdjustmentThreshold = 7;
  
  double productivityIndex = this->productivityIndex();
  double finalScale = dueProductivityIndex - productivityIndex;
  double scaleFactor = 0.0;

  int daysDifference = numberOfDaysDifference(m_dueDate, TaskDate::validDate());
  if (daysDifference <= dueDateAdjustmentThreshold) {
    // If we're at or past the due date, clamp
    if (daysDifference <= 0) {
      scaleFactor = 1.0;
    }
    else {
      scaleFactor = static_cast<double>(dueDateAdjustmentThreshold + 1 - daysDifference) / (dueDateAdjustmentThreshold + 1);
    }
  }
  // If we're too far before the due date, don't adjust the index
  else {
    scaleFactor = 0.0;
  }
  
  return productivityIndex + scaleFactor*finalScale;
}

std::string durationStringForTask(const Task& task)
{
  bool noMinimum = (task.minimumDuration().hours()) == 0 && (task.minimumDuration().minutes() == 0);
  bool noMaximum = task.maximumDuration().isInfinite();
  
  if (noMinimum && noMaximum) {
    return "N/A";
  }
  else if (noMinimum) {
    return "<" + durationString(task.maximumDuration());
  }
  else if (noMaximum) {
    return ">" + durationString(task.minimumDuration());
  }
  else {
    return durationString(task.minimumDuration()) + " - " + durationString(task.maximumDuration());
  }
}

} // namespace probsched
