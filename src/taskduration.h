//
// File: taskduration.h
// Author: Spencer Phippen
//
// Contains class definition for TaskDuration, a class that represents the duration of a task.

#ifndef _PROBSCHED_TASK_DURATION_H_
#define _PROBSCHED_TASK_DURATION_H_

#include <string>

namespace probsched
{

// A class that represents the duration of a task.
class TaskDuration
{
 public:
  // Creates and returns a finite TaskDuration with the given number of hours and minutes.
  //
  // @param hours The number of hours in the duration
  // @param minute The number of minutes in the duration
  static TaskDuration finiteDuration(int hours, int minutes);

  // Creates and returns a finite TaskDuration with the given number of hours.
  //
  // @param hours The number of hours in the duration - is split into hours and minutes internally
  static TaskDuration finiteDuration(double hours);

  // Creates and returns an infinite TaskDuration.
  static TaskDuration infiniteDuration();

  // Returns true if the duration is infinite, false otherwise.
  bool isInfinite() const;

  // Returns the number of hours in this duration, or -1 if the duration is infinite.
  int hours() const;
  // Sets the number of hours in this duration.
  // Call only on finite durations.
  // @param hours The new number of hours.
  //              Must be >= 0.
  void setHours(int hours);

  // Returns the number of minutes in this duration, or -1 if the duration is infinite.
  int minutes() const;
  // Sets the number of minutes in this duration.
  // Call only on finite durations.
  // @param minutes The new number of minutes.
  //                Must be >= 0.
  void setMinutes(int minutes);
 private:
  TaskDuration(int hours, int minutes, bool m_isInfinite);
  int m_hours;
  int m_minutes;
  bool m_isInfinite;
};

bool operator==(const TaskDuration& td1, const TaskDuration& td2);
bool operator<(const TaskDuration& td1, const TaskDuration& td2);
bool operator<=(const TaskDuration& td1, const TaskDuration& td2);
bool operator>(const TaskDuration& td1, const TaskDuration& td2);
bool operator>=(const TaskDuration& td1, const TaskDuration& td2);

std::string durationString(const TaskDuration& td);

} // namespace probsched

#endif // _PROBSCHED_TASK_DURATION_H_
