// 
// File: cmdlineoptionparser.h
// Author: Spencer Phippen
// 
// Contains class definition for CmdLineOptionParser, a class that helps parse command line options.

#ifndef _PROBSCHED_CMDLINE_OPTION_PARSER_H_
#define _PROBSCHED_CMDLINE_OPTION_PARSER_H_

#include <string>
#include <map>
#include <vector>

namespace probsched
{

// A class that eases in the parsing and validation of command line options.
class CmdLineOptionParser
{
 public:
  CmdLineOptionParser();

  // Adds a command line option with the given short flag and long option.
  // At least one of the short or long flags must be specified.
  // Option arguments may be passed in the same string for short flags, and must be passed in the next argument for long options. For example:
  //   For a 'w' flag:
  //     "-w15" is valid
  //     "-w 15" is valid
  //   For a "longarg" option:
  //     "--longarg hello" is valid
  //     "--longarghello" is not valid (parses to a "longarghello" option)
  //     "-longarghello" is not valid (parses to 'l' short flag with "ongarghello" argument)
  //
  // @param flag The short flag for the option, e.g. 'w' in "-w15".
  //             Pass '\0' for no short flag.
  // @param longFlag The long flag for the option, e.g. "force" in "--force"
  //                   Pass "" (empty string) for no long flag.
  // @param hasArgument Pass true for the flag to have an argument, false for it to have no argument
  // @return The positive (>0) id associated with this argument, or -1 if both flags were '\0' and "", respectively.
  int addOption(char flag, std::string longFlag, bool hasArgument);

  // Returns the number of mandatory arguments.
  // Defaults to -1 (any number of arguments).
  int mandatoryArgumentCount() const;
  // Set the number of mandatory arguments.
  // @param count The number of mandatory arguments
  //              Must be >= -1.
  //              -1 indicates "any number of arguments allowed"
  void setMandatoryArgumentCount(int count);

  // Returns true if the parser is set to ignore unrecognized options.
  // Defaults to false.
  bool doesIgnoreUnrecognizedOptions() const;
  // Sets whether the parser is set to ignore unrecognized options.
  //
  // @param ignore If true, unrecognized options are ignored.
  //               If false, unrecognized options make a parse unsuccessful.
  void ignoreUnrecognizedOptions(bool ignore);

  // Returns true if the parser is set to ignore repeated options.
  // Defaults to false.
  bool doesIgnoreRepeatedOptions() const;
  // Sets whether the parser is set to ignore repeated options.
  //
  // @param ignore If true, repeated options are ignored.
  //               If false, repeated options make a parse unsuccessful.
  void ignoreRepeatedOptions(bool ignore);

  // Processes arguments from the command line.
  // After this function call, |successfulParse| can be called to determine whether the parse was successful.
  // Options can be extracted with |getArgumentForOption|.
  // If the parse was successful, |argc| and |argv| can be used to iterate over non-option arguments after the function returns.
  //
  // @param argc The number of command line arguments. Pass in address of |argc| from |main|.
  //             Assuming successful parse, contains number of non-option arguments after call to this function.
  //             Contents undefined if parse is unsuccessful.
  // @param argv The array of command line arguments. Pass in |argv| from |main|.
  //             Assuming successful parse, contains all non-option arguments after call to this function.
  //             Contents undefined if parse is unsuccessful.
  void parseArguments(int* argc, char* argv[]);

  // Returns true if a previous call to |processArgs| successfully validated the arguments.
  // Call only after a call to |processArgs|.
  bool parseWasSuccessful() const;
  // Returns an error string describing errors that occured during parsing.
  // Returns "" (empty string) if there were no errors.
  // Call only after a call to |processArgs|.
  std::string parseError() const;

  // Returns the id for the option associated with the short flag |flag|.
  //
  // @param flag Short flag associated with the desired option.
  // @return The id associated with the given short flag's option, or -1 if no such option exists.
  int idForOptionWithShortFlag(char flag) const;

  // Returns the id for the option associated with the long flag |longFlag|.
  //
  // @param longFlag Long flag associated with the desired option.
  // @return The id associated with the given long flag's option, or -1 if no such option exists.
  int idForOptionWithLongFlag(std::string longFlag) const;

  // Returns true if the option associated with the given id was passed, false otherwise.
  //
  // @param optionId The id of the desired option.
  bool optionWithIdWasPassed(int optionId) const;

  // Returns the argument for the option with option id |optionId|.
  //
  // @param optionId The id for the desired option.
  // @return The argument for the option specified, or "" if:
  //           - No argument was passed
  //           - The option has no argument
  //           - The option id is invalid
  std::string argumentForOptionWithId(int optionId) const;

 private:
  void parseShortOption(int* optIndex, int* argc, char* argv[]);
  void parseLongOption(int* optIndex, int* argc, char* argv[]);

  std::map<char, int> m_shortToId;
  std::map<std::string, int> m_longToId;
  std::map<int, bool> m_idToHasArgument;
  std::map<int, std::string> m_idToArgument;
  int m_mandatoryArgumentCount;
  bool m_ignoreUnrecognizedOptions;
  bool m_ignoreRepeatedOptions;
  int m_nextId;

  bool m_hasProcessed;
  bool m_successfulParse;
  std::string m_errorString;
};

} // namespace probsched

#endif // _PROBSCHED_CMDLINE_OPTION_PARSER_H_
