//
// File: hipretime.h
// Author: Spencer Phippen
//
// Contains declarations for high-precision time related functions.

#ifndef _PROBSCHED_HI_PRE_TIME_H_
#define _PROBSCHED_HI_PRE_TIME_H_

#include <cstdint>

namespace probsched
{

// Returns a value that changes with time at some small frequency.
// Used for seeding RNGs.
uint32_t highPrecisionTime();

} // namespace probsched

#endif // _PROBSCHED_HI_PRE_TIME_H_