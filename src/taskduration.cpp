//
// File: taskduration.cpp
// Author: Spencer Phippen
//
// Contains function definitions for the TaskDuration class.

#include "taskduration.h"

#include <cassert>
#include <sstream>
#include <cmath>

using std::ostringstream;

namespace probsched
{

TaskDuration TaskDuration::finiteDuration(int hours, int minutes)
{
  // Both hours and minutes must be non-negative
  assert(hours >= 0 && minutes >= 0);
  return TaskDuration(hours, minutes, false);
}

TaskDuration TaskDuration::finiteDuration(double hours)
{
  // The number of hours must be non-negative
  assert(hours >= 0);
  
  double hoursIntegral;
  double hoursFractional = modf(hours, &hoursIntegral);
  
  int realHours = static_cast<int>(hoursIntegral);
  // Round to nearest minute
  int realMinutes = static_cast<int>((hoursFractional * 60.0) + 0.5);
  
  return TaskDuration(realHours, realMinutes, false);
}

TaskDuration TaskDuration::infiniteDuration()
{
  return TaskDuration(-1, -1, true);
}

bool TaskDuration::isInfinite() const
{
  return m_isInfinite;
}

int TaskDuration::hours() const
{
  return m_hours;
}
void TaskDuration::setHours(int hours)
{
  assert(!m_isInfinite);
  m_hours = hours;
}

int TaskDuration::minutes() const
{
  return m_minutes;
}
void TaskDuration::setMinutes(int minutes)
{
  assert(!m_isInfinite);
  m_minutes = minutes;
}

TaskDuration::TaskDuration(int hours, int minutes, bool isInfinite) : m_hours(hours), m_minutes(minutes), m_isInfinite(isInfinite) { }

bool operator==(const TaskDuration& td1, const TaskDuration& td2)
{
  bool firstInfinite = td1.isInfinite();
  bool secondInfinite = td2.isInfinite();

  // If one is infinite and the other is finite
  if (firstInfinite != secondInfinite) {
    return false;
  }
  // If both are infinite
  else if (firstInfinite) {
    return true;
  }
  // If both are finite
  else {
    return (td1.hours() == td2.hours()) && (td1.minutes() == td2.minutes());
  }
}

bool operator<(const TaskDuration& td1, const TaskDuration& td2)
{
  bool firstInfinite = td1.isInfinite();
  bool secondInfinite = td2.isInfinite();

  // If the first is infinite, it can't be strictly less than the second
  if (firstInfinite) {
    return false;
  }
  // If the first is finite and the second is infinite
  else if (secondInfinite) {
    return true;
  }
  // If they're both finite
  else {
    int firstHours = td1.hours();
    int secondHours = td2.hours();
    if (firstHours < secondHours) {
      return true;
    }
    else if (firstHours > secondHours) {
      return false;
    }
    // If they both have the same number of hours
    else {
      return td1.minutes() < td2.minutes();
    }
  }
}

bool operator<=(const TaskDuration& td1, const TaskDuration& td2)
{
  return (td1 == td2) || (td1 < td2);
}

bool operator>(const TaskDuration& td1, const TaskDuration& td2)
{
  return td2 < td1;
}

bool operator>=(const TaskDuration& td1, const TaskDuration& td2)
{
  return td2 <= td1;
}

std::string durationString(const TaskDuration& td)
{
  if (td.isInfinite()) {
    return "infinity";
  }
  else {
    ostringstream duration;
    if (td.hours() != 0) {
      duration << td.hours() << "h";
    }
    duration << td.minutes() << "m";
    return duration.str();
  }
}

} // namespace probsched
